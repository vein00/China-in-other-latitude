var map = L.map('mapid', {
    center: [0, 0],
    zoom: 1
});

L.tileLayer.chinaProvider('TianDiTu.Normal.Map',{maxZoom:12,minZoom:1}).addTo(map);
L.tileLayer.chinaProvider('TianDiTu.Normal.Annotion',{maxZoom:12,minZoom:1}).addTo(map);

L.geoJson(DATA, {
    style: function (feature) {
        return {color: feature.properties.color};
    },
    onEachFeature: function (feature, layer) {
        layer.bindPopup(feature.properties.description);
    }
}).addTo(map);
